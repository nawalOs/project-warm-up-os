#include "syscall.h"
#include "traps.h"
#include "types.h"
#include "user.h"

int getsyscallinfo2 (void)
{
	int ppid;
	asm volatile ("int %1" : "=a" (ppid): "i" (T_SYSCALL) ,
"a" (SYS_getsyscallinfo));
	return ppid;
}
int main(void) { 
int cp = fork(); 
if (cp < 0)
 printf(1,"Fork_failed_%\n",cp); 

else if (cp > 0) {

 printf(1,"The process 'cp' has been created",getpid(),getsyscallinfo2()); 



wait();

}
else{

printf(1,"i am parent process",getpid(), getsyscallinfo2()); 
}

exit();

}
